from random import randint

name = input("Hi, What is your name?")
guesses = 1
total_guesses = 5

for i in range(total_guesses):
    guessed_month = randint(1, 12)
    guessed_year = randint(1924, 2004)
    print("Guess", guesses, ":", name, "were you born in", guessed_month, "/", guessed_year,"?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        if guesses == 5:
            print("I have other things to do. Good bye.")
        else:
            print("Drat! Lemme try again!")
            guesses += 1

